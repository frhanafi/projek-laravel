<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('halaman.register');
    }

    public function kirim(Request $request){
        $firstname = $request['first name'];
        $lastname = $request['last name'];
        return view('halaman.hello', compact('firstname','lastname'));
    }
}
