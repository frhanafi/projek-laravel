@extends('layout.master')

@section('judul')
Halaman Home
@endsection

@section('content')
    <h1>Media Online</h1>
    <h2>Sosial Media Developer</h2>
    <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
    <h2>Benefit join di Media Online</h2>
    <ol>
        <li>Mendapatkan motivasi dari sesama para Developer</li>
        <li>Sharing knowledge</li>
        <li>Dibuat oleh calon Developer terbaik</li>
    </ol>
    <h2>Cara bergabung ke Media Online</h2>
    <ul>
        <li>Menggunakan website ini</li>
        <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
        <li>Selesai</li>
    </ul>
@endsection